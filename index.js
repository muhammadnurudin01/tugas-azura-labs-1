let express = require("express");
let bodyParser = require("body-parser");
let categoryModel = require("./models").category;
let productModel = require("./models").product;
let sellerModel = require("./models").seller;

let app = express();
//fetch form data from the request.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
	//this will join the tables and display data
	productModel
		.findAll({ include: [{ model: categoryModel }, { model: sellerModel }] })
		.then(function (data) {
			res.json({ da: data });
		})
		.catch(function (error) {
			res.json({ er: error });
		});
});

app.get("/productbycategory", (req, res) => {
	//this will join the tables and display data
	productModel
		.findAll({
			include: [{ model: categoryModel }],
			where: {
				categoryId: req.query.id,
			},
		})
		.then(function (data) {
			res.json({ da: data });
		})
		.catch(function (error) {
			res.json({ er: error });
		});
});

app.get("/category", (req, res) => {
	//this will join the tables and display data
	categoryModel
		.findAll()
		.then(function (data) {
			res.json({ da: data });
		})
		.catch(function (error) {
			res.json({ er: error });
		});
});

app.get("/product", (req, res) => {
	//this will join the tables and display data
	productModel
		.findOne({
			include: [{ model: categoryModel }, { model: sellerModel }],
			where: {
				id: req.query.id,
			},
		})
		.then(function (data) {
			res.json({ da: data });
		})
		.catch(function (error) {
			res.json({ er: error });
		});
});

//assign the port
let port = 3200;
app.listen(port, () => console.log("server running at port " + port));
